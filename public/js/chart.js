import { graph_temp } from './graphique/graph-temp.js'
import { graph_hum } from './graphique/graph-hum.js'
import { myHumidity } from './graphique/humidity.js'
import { myTemperature } from './graphique/temperature.js'

import { myComparatif } from './graphique/comparatif.js'


const data_temp = JSON.parse(document.getElementById('myTemp').getAttribute('temperature'))
const data_hum = JSON.parse(document.getElementById('myTemp').getAttribute('humidity'))

var myDataGraphTemperature = graph_temp(data_temp)
var myDataTemperature = myTemperature(data_temp)

var myDataGraphHumidity = graph_hum(data_hum)
var myDataHumidity = myHumidity(data_hum)

var myDataComparatif = myComparatif(data_temp, data_hum)