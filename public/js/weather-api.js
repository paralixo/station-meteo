const axios = require("axios");

class WeatherApi {
    static apiKey = "94f938aa710849228c0145526210503";
    static city = "Bordeaux";
    static apiUrl = `http://api.weatherapi.com/v1/current.json?key=${this.apiKey}&q=${this.city}&aqi=no`;

    static async getData() {
        const response = await axios.get(this.apiUrl);
        return {
            temperature: response.data.current.temp_c,
            condition: response.data.current.condition.text
        }
    }
}

module.exports = WeatherApi;