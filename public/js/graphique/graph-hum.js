var ctx = document.getElementById('graph_hum').getContext('2d');
function graph_hum(humidities) {
    const data = humidities.map(humidity=>humidity.HUMIDITY)
    const dataHeure = humidities.map(humidity=>{
        var list = humidity.DATE
        var e = list.split(/[- :T]/)
        var d = e[2] + "/" + e[1] + " " + e[3] + ":" + e[4]
        return d
    })
    data.reverse()
    dataHeure.reverse()
    return new Chart(ctx, {
        type: 'line',
        data: {
            labels: dataHeure,
            datasets: [{
                label: 'Humidité en %',
                data,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        if (value) {
                            return "Humidité : " + value + "%";
                        }
                    }
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        maxTicksLimit: 10
                    }
                }],
                yAxes: [{
                    ticks: {
                        max: 100,
                        beginAtZero: true
                    }
                }]
            }
        }
    });
} 

export { graph_hum }