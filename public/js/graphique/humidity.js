var ctx = document.getElementById('myHumidity').getContext('2d');
function myHumidity(humiditie) {
    const data = humiditie.map(humidity=>humidity.HUMIDITY)
    const labelH = data[0]
    return new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Aujourd'hui"],
            datasets: [{
                label: 'Humidité',
                data,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        if (value) {
                            return data.datasets[tooltipItem.datasetIndex].label + " : " + value + "%";
                        }
                    }
                }
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        fontStyle: "bold",
                        fontColor: "rgba(54, 162, 235, 1)",
                        labelString: labelH +'%'
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

export { myHumidity };