var ctx = document.getElementById('myTemperature').getContext('2d');
function myTemperature(temperatures) {
    const data = temperatures.map(temperature=>temperature.TEMPERATURE)
    const labelT = data[0]
    return new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Aujourd'hui"],
            datasets: [{
                label: 'Température',
                data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
                }]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        if (value) {
                            return data.datasets[tooltipItem.datasetIndex].label + " : " + value + "°C";
                        }
                    }
                }
            },
            scales: {
                xAxes: [{
                     scaleLabel: {
                        display: true,
                        fontStyle: "bold",
                        fontColor: "rgba(255, 99, 132, 1)",
                        labelString: labelT +'°C'
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    });
}

export { myTemperature };