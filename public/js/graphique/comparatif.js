var ctx = document.getElementById('myComparatif').getContext('2d');
function myComparatif(temperature, humidities) {
    const dataT = temperature.map(temperature=>temperature.TEMPERATURE);
    const dataH = humidities.map(humidity=>humidity.HUMIDITY);
    dataT.reverse()
    dataH.reverse()

    const dataHeure = temperature.map(humidity=> {
        var list = humidity.DATE
        var e = list.split(/[- :T]/)
        var d = e[2] + "/" + e[1] + " " + e[3] + ":" + e[4]
        return d
    });
    dataHeure.reverse();

    return new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Température °C',
                data: dataT,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
            }, {
                label: 'Humidité %',
                data: dataH,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                ],
            }],
            labels: dataHeure,
        },
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    ticks: {
                        maxTicksLimit: 10
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    },
                    display: true,
                }]
            },
        }
    });
}

export { myComparatif }