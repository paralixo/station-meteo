var ctx = document.getElementById('graph_temp').getContext('2d');
function graph_temp(temperatures) {
    const data = temperatures.map(temperature=>temperature.TEMPERATURE)
    const dataHeure = temperatures.map(temperature=>{
        var list = temperature.DATE
        var e = list.split(/[- :T]/)
        var d = e[2] + "/" + e[1] + " " + e[3] + ":" + e[4]
        return d
    })

    data.reverse()
    dataHeure.reverse()
    return new Chart(ctx, {
        type: 'line',
        data: {
            labels: dataHeure,
            datasets: [{
                label: 'Température en °C',
                data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1,
            }]
        },
        options: {
            tooltips: {
                data:{
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        if (value) {
                            return  "Température : " + value + "°C";
                        }
                    }
                },
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        if (value) {
                            return  "Température : " + value + "°C";
                        }
                    }
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        maxTicksLimit: 10
                    }
                }],
                yAxes: [{
                    ticks: {
                        max: 30,
                        beginAtZero: true
                    }
                }]
            }
        }
    });
} 

export { graph_temp };