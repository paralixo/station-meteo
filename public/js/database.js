const {Sequelize} = require('sequelize');
const UserModel = require('./models/user')
const HumidityModel = require('./models/humidity')
const TemperatureModel = require('./models/temperature')

class Database {
    constructor() {
        this.sequelize = new Sequelize('meteo', 'fmeteo', 'Meteo123', {
            host: 'lf426346-001.dbaas.ovh.net',
            port: 35275,
            dialect: 'mysql',
            logging: false
        });
        this.init_models()
    }

    init_models() {
        this.Humidity = HumidityModel(this.sequelize)
        this.Humidity.sync()
        this.Temperature = TemperatureModel(this.sequelize)
        this.Temperature.sync()
        this.User = UserModel(this.sequelize)
        this.User.sync()
    }

    async synchronize() {
        await this.sequelize.sync();
    }
}

const db = new Database()

module.exports = {Database: db, Humidity: db.Humidity, Temperature: db.Temperature, User: db.User};