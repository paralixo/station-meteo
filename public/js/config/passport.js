const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const {User} = require('../database')

module.exports = function (passport) {
    passport.use(
        new LocalStrategy({usernameField: 'pseudo'}, (pseudo, password, done) => {
            //match user
            User.findOne({where: {NAME: pseudo}})
                .then((user) => {
                    if (!user) {
                        return done(null, false, {'message': 'Ce pseudo n\'existe pas'});
                    }
                    //match pass
                    bcrypt.compare(password, user.PASSWORD, (err, isMatch) => {
                        if (err) throw err;

                        if (isMatch) {
                            return done(null, user);
                        } else {
                            return done(null, false, {'message': 'Mot de passe incorrecte'});
                        }
                    })
                })
                .catch((err) => {
                    console.log(err)
                })
        })
    )
    passport.serializeUser(function (user, done) {
        done(null, user.ID);
    });

    passport.deserializeUser(function (id, done) {
        User.findOne({where: {ID: id}}).then((user) => {
            done(null, user);
        }).catch(done);
    });
};