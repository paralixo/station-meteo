const cron = require('node-cron');
const WeatherApi = require("./weather-api");
const MqttBroker = require('./mqtt/mqtt-broker')

const EVERY_THREE_HOURS = '0 */3 * * *'

cron.schedule(EVERY_THREE_HOURS, async () => {
    const weatherApiData = await WeatherApi.getData();
    console.log('I published weather api data to mqtt broker');

    MqttBroker.PublishOnOutdoorTemperature(weatherApiData['temperature'].toString())
    MqttBroker.PublishOnOutdoorWeather(weatherApiData['condition'])

})