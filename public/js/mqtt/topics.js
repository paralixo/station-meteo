const HOUSE_INFORMATION = 'house/'

const INIT = '/init'
const TEMPERATURE = '/temperature'
const TEMPERATURE_ALARME = TEMPERATURE + '/alarme'
const HUMIDITY = '/humidity'
const HUMIDITY_ALARME = HUMIDITY + '/alarme'

const MAX = '/max'
const MIN = '/min'

const OUTDOOR_TEMPERATURE = 'temperature'
const OUTDOOR_WEATHER = 'weather'

const OUTDOOR = 'outdoor/'

const INDOOR_TOPICS = [
    TEMPERATURE,
    HUMIDITY,
    INIT
]

const OUTDOOR_TOPIC = [
    OUTDOOR_TEMPERATURE,
    OUTDOOR_WEATHER
]

module.exports = {
    HOUSE_INFORMATION,
    TEMPERATURE,
    TEMPERATURE_ALARME,
    HUMIDITY,
    HUMIDITY_ALARME,
    OUTDOOR_TEMPERATURE,
    OUTDOOR_WEATHER,
    INDOOR_TOPICS,
    OUTDOOR_TOPIC,
    OUTDOOR,
    MAX,
    MIN,
    INIT
};