const mqtt = require('mqtt');
const {
    INDOOR_TOPICS,
    OUTDOOR_TOPIC,
    HOUSE_INFORMATION,
    OUTDOOR,
    OUTDOOR_WEATHER,
    OUTDOOR_TEMPERATURE,
    TEMPERATURE,
    INIT,
    HUMIDITY,
    TEMPERATURE_ALARME,
    HUMIDITY_ALARME,
    MIN,
    MAX
} = require('./topics')
const WeatherApi = require("../weather-api");

const fs = require('fs')
const {User, Humidity, Temperature} = require('../database')


class MqttBroker {
    constructor() {
        const connection_infos = this.GetConnectionInfo();
        this.broker_addr = 'mqtt://' + connection_infos['broker_addr'];
        this.connection_info = {
            clientId: connection_infos['clientId'],
            username: connection_infos['username'],
            password: connection_infos['password']
        }
        this.client = mqtt.connect(this.broker_addr, this.connection_info);
        User.findAll().then((users) => {
            this.Subscribe(users)
        })
        console.log('MQTT Broker initialized')
    };


    ConnectToBroker() {
        console.log(`Trying to connect on Broker ${this.broker_addr}`)
        this.client.on('connect', () => {
            console.log(`Connected to ${this.broker_addr}`);
            this.Publish('test', 'test connection')
        });

        this.client.on('close', () => {
            console.log('Client closed.')
        });

        this.client.on('error', (error) => {
            console.log(`Can't connect on Broker ${this.broker_addr} with error : ` + error);
            process.exit(1)
        });
    }

    GetConnectionInfo() {
        let raw_datas = fs.readFileSync(__dirname + '/connection-infos.json');
        return JSON.parse(raw_datas);
    }


    UpdateUserHouseInformation(topic, message) {
        topic = topic.split('/')
        const user_mac = topic[1]
        const subject = topic[2]
        User.findOne({where: {MAC: user_mac}}).then(async (user) => {
            const humidity_subject = HUMIDITY.split('/')[1]
            const temperature_subject = TEMPERATURE.split('/')[1]
            const init_subject = INIT.split('/')[1]
            if (subject === humidity_subject) {
                const humidity = new Humidity({
                    USER_ID: user.ID,
                    HUMIDITY: message,
                })
                humidity.save()
            } else if (subject === temperature_subject) {
                const temperature = new Temperature({
                    USER_ID: user.ID,
                    TEMPERATURE: message,
                })
                temperature.save()
            } else if (subject === init_subject) {
                const weatherApiData = await WeatherApi.getData();
                this.PublishOnOutdoorTemperature(weatherApiData['temperature'].toString())
                this.PublishOnOutdoorWeather(weatherApiData['condition'])

                this.PublishOnTemperatureAlarmMax(user_mac, user.MAXIMAL_TEMPERATURE.toString())
                this.PublishOnTemperatureAlarmMin(user_mac, user.MINIMAL_TEMPERATURE.toString())

                this.PublishOnTemperatureHumidityMax(user_mac, user.MAXIMAL_HUMIDITY.toString())
                this.PublishOnTemperatureHumidityMin(user_mac, user.MINIMAL_HUMIDITY.toString())
            }
        })
    }

    GetTopic(place, user_mac = '', topic = '') {
        return place + user_mac + topic
    }


    Publish(topic, message) {
        console.log(`[${topic}] Trying to send message [${message}]`);
        this.client.publish(topic, message);
        console.log(`[${topic}] The message [${message}] has been sent`)
    }

    PublishOnOutdoor(message) {
        this.Publish(OUTDOOR, message)
    }

    PublishOnOutdoorWeather(message) {
        this.Publish(this.GetTopic(OUTDOOR, OUTDOOR_WEATHER), message)
    }

    PublishOnOutdoorTemperature(message) {
        this.Publish(this.GetTopic(OUTDOOR, OUTDOOR_TEMPERATURE), message)
    }

    PublishOnTemperatureAlarmMax(user_mac, message) {
        this.Publish(this.GetTopic(HOUSE_INFORMATION, user_mac, TEMPERATURE_ALARME + MAX), message)
    }

    PublishOnTemperatureAlarmMin(user_mac, message) {
        this.Publish(this.GetTopic(HOUSE_INFORMATION, user_mac, TEMPERATURE_ALARME + MIN), message)
    }

    PublishOnTemperatureHumidityMax(user_mac, message) {
        this.Publish(this.GetTopic(HOUSE_INFORMATION, user_mac, HUMIDITY_ALARME + MAX), message)
    }

    PublishOnTemperatureHumidityMin(user_mac, message) {
        this.Publish(this.GetTopic(HOUSE_INFORMATION, user_mac, HUMIDITY_ALARME + MIN), message)
    }

    Subscribe(users) {
        this.client.on('message', (topic, message) => {
            console.log(`[${topic}] Message received [${message}]`)
            this.UpdateUserHouseInformation(topic, message)
        })
        users.forEach((user) => {
            for (let topic of INDOOR_TOPICS) {
                const user_topic = this.GetTopic(HOUSE_INFORMATION, user.dataValues.MAC, topic)
                this.client.subscribe(user_topic)
                console.log(`[${user.dataValues.MAC}] Subscribing to topic [${user_topic}]`);
            }
            for (let topic of OUTDOOR_TOPIC) {
                const user_topic = this.GetTopic(OUTDOOR, topic)
                this.client.subscribe(user_topic)
                console.log(`[${user.dataValues.MAC}] Subscribing to topic [${user_topic}]`);
            }
        })
    }
}

const broker = new MqttBroker()
broker.ConnectToBroker()


module.exports = broker