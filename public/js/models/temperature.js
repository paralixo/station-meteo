const DataTypes = require('sequelize');
const sequelize = require('../database')

module.exports = (sequelize) => {
    return sequelize.define('TEMPERATURE', {
            ID: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            USER_ID: {
                type: DataTypes.INTEGER
            },
            TEMPERATURE: {
                type: DataTypes.DECIMAL(5, 2)
            },
            DATE: {
                type: DataTypes.DATE,
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        {
            tableName: 'TEMPERATURE',
            timestamps: false
        })
};