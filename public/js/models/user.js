const DataTypes = require('sequelize');


module.exports = (sequelize) => {
    return sequelize.define('USERS', {
            ID: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            NAME: {
                type: DataTypes.STRING
            },
            PASSWORD: {
                type: DataTypes.STRING
            },
            MAC: {
                type: DataTypes.STRING
            },
            MAXIMAL_TEMPERATURE: {
                type: DataTypes.FLOAT
            },
            MINIMAL_TEMPERATURE: {
                type: DataTypes.FLOAT
            },
            MAXIMAL_HUMIDITY: {
                type: DataTypes.FLOAT
            },
            MINIMAL_HUMIDITY: {
                type: DataTypes.FLOAT
            },

        },
        {
            tableName: 'USERS',
            timestamps: false
        })
};