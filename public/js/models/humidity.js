const DataTypes = require('sequelize');
const sequelize = require('../database')

module.exports = (sequelize) => {
    return sequelize.define('HUMIDITY', {
            ID: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            USER_ID: {
                type: DataTypes.INTEGER
            },
            HUMIDITY: {
                type: DataTypes.DECIMAL(5, 2)
            },
            DATE: {
                type: DataTypes.DATE,
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        {
            tableName: 'HUMIDITY',
            timestamps: false
        })
};