const {Database, Temperature, Humidity} = require('./public/js/database')

Database.synchronize().then(() => {

    const express = require("express");
    const bodyParser = require("body-parser")
    const session = require('express-session');
    const flash = require('connect-flash');
    const bcrypt = require('bcrypt');
    const {User} = require('./public/js/database')
    const passport = require('passport');
    require("./public/js/config/passport")(passport)
    const mqttBroker = require('./public/js/mqtt/mqtt-broker')
    require('./public/js/cron');

    const app = express();

    app.use(express.static("public"));
    app.use(bodyParser.urlencoded({extended: true}))
    app.use(session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());
    app.use(function (req, res, next) {
        res.locals.success_messages = req.flash('success_messages');
        res.locals.error_messages = req.flash('error_messages');
        res.locals.login_error = req.flash('error')
        next();
    });
    app.set("view engine", "ejs");
    app.set("views", "./views");

    app.listen(3000);

    app.get("/", async function (request, response) {

        if (request.user) {
            const humidity = JSON.stringify(await Humidity.findAll({
                where: {USER_ID: request.user.ID},
                order: [
                    ['ID', 'DESC'],
                ]
            }), null, 2)
            const temperature = JSON.stringify(await Temperature.findAll({
                where: {USER_ID: request.user.ID},
                order: [
                    ['ID', 'DESC'],
                ]
            }), null, 2)
            response.render("index", {user: request.user, temperature, humidity});
        } else {
            response.redirect("/login");
        }
    });

    app.get("/login", function (request, response) {
        response.render("login", {user: request.user});
    });

    app.get("/signup", function (request, response) {
        response.render("signup", {user: request.user});
    });

    app.post('/signup', async (request, response) => {
        if (request.body.password === request.body.passwordVerification) {
            let user = await User.findOne({where: {NAME: request.body.pseudo}})
            if (user) {
                request.flash('error_messages', 'L\'utilisateur existe déjà')
                response.redirect('/signup')
            } else {
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(request.body.password, salt, (err, hash) => {
                        if (err) throw err;
                        const user = new User({
                            NAME: request.body.pseudo,
                            MAC: request.body.mac,
                            PASSWORD: hash
                        });
                        user.save().then(() => {
                            request.flash('success_messages', 'Vous êtes maintenant enregistré !')
                            response.redirect('/login')
                        })
                    })
                });
            }
        } else {
            request.flash('error_messages', 'Les mots de passe sont différent')
            response.redirect('/signup')
        }
    })

    app.post('/login', async (request, response, next) => {
        passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true,
        })(request, response, next)

    })

    app.get('/logout', (request, response, next) => {
        request.logout();
        response.redirect('/')

    })

    app.post('/', async (request, response) => {
        if (request.body.alarm_temp_max) {
            mqttBroker.PublishOnTemperatureAlarmMax(request.user.MAC, request.body.alarm_temp_max)
            request.user.MAXIMAL_TEMPERATURE = request.body.alarm_temp_max
            await request.user.save()
        }
        if (request.body.alarm_temp_min) {
            mqttBroker.PublishOnTemperatureAlarmMin(request.user.MAC, request.body.alarm_temp_min)
            request.user.MINIMAL_TEMPERATURE = request.body.alarm_temp_min
            await request.user.save()
        }
        if (request.body.alarm_hum_max) {
            mqttBroker.PublishOnTemperatureHumidityMax(request.user.MAC, request.body.alarm_hum_max)
            request.user.MAXIMAL_HUMIDITY = request.body.alarm_hum_max
            await request.user.save()
        }
        if (request.body.alarm_hum_min) {
            mqttBroker.PublishOnTemperatureHumidityMin(request.user.MAC, request.body.alarm_hum_min)
            request.user.MINIMAL_HUMIDITY = request.body.alarm_hum_min
            await request.user.save()
        }
        response.redirect('/')
    })
})

