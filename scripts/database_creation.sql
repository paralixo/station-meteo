CREATE TABLE `meteo`.`USERS` ( 
    `ID` INT NOT NULL AUTO_INCREMENT , 
    `NAME` VARCHAR(64) NOT NULL , 
    `PASSWORD` VARCHAR(64) NOT NULL , 
    `MAC` VARCHAR(64) NOT NULL UNIQUE ,
	`MINIMAL_TEMPERATURE` FLOAT DEFAULT 14,
	`MAXIMAL_TEMPERATURE` FLOAT DEFAULT 30,
	`MINIMAL_HUMIDITY` FLOAT DEFAULT 20,
	`MAXIMAL_HUMIDITY` FLOAT DEFAULT 50,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB;
create table HUMIDTY
(
	ID int auto_increment,
	USER_ID int not null,
	HUMIDITY decimal(5,2) not null,
	DATE datetime default CURRENT_TIMESTAMP not null,
	constraint HUMIDTY_pk
		primary key (ID),
	constraint HUMIDTY_USERS_ID_fk
		foreign key (USER_ID) references USERS (ID)
) ENGINE = InnoDB;
create table TEMPERATURE
(
	ID int auto_increment,
	USER_ID int not null,
	TEMPERATURE decimal(5,2) not null,
	DATE datetime default CURRENT_TIMESTAMP not null,
	constraint TEMPERATURE_pk
		primary key (ID),
	constraint TEMPERATURe_USERS_ID_fk
		foreign key (USER_ID) references USERS (ID)
) ENGINE = InnoDB;